use std::{fs::File, io::prelude::*};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

const TARGET: usize = 19_690_720;

/// Simple macro to check that a a pointer is valid
macro_rules! assert_in_bounds {
    ($vector:expr, $index:expr, $fmt_str:literal, $($args:expr),+) => {
        assert!($index < $vector.len(), $fmt_str, $($args),+)
    };
}

fn main() -> Result<()> {
    let mut file = File::open("input")?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;

    // Part 1
    let mut program = create_program(&input, 12, 2)?;
    program.execute();
    println!("Part 1: {}", program.memory[0]);

    // Part 2 - Brute force
    let mut noun = 0;
    let mut verb = 0;
    let mut result = test_pair(&input, noun, verb)?;
    while result <= TARGET {
        noun += 1;
        result = test_pair(&input, noun + 1, verb)?;
    }
    result = test_pair(&input, noun, verb)?;
    while result <= TARGET {
        verb += 1;
        result = test_pair(&input, noun, verb + 1)?;
    }
    result = test_pair(&input, noun, verb)?;
    assert_eq!(result, TARGET, "Couldn't find result");
    println!("Part 2: {}", 100 * noun + verb);

    Ok(())
}

fn test_pair(input: &str, noun: usize, verb: usize) -> Result<usize> {
    let mut program = create_program(input, noun, verb)?;
    program.execute();
    Ok(program.memory[0])
}

fn create_program(input: &str, noun: usize, verb: usize) -> Result<IntcodeComputer> {
    let mut program = IntcodeComputer {
        memory: input
            .replace('\n', "")
            .split(',')
            .map(str::parse)
            .collect::<std::result::Result<_, _>>()?,
        pc: 0,
        halted: false,
    };

    program.memory[1] = noun;
    program.memory[2] = verb;

    Ok(program)
}

#[derive(Debug)]
struct IntcodeComputer {
    memory: Vec<usize>,
    pc: usize,
    halted: bool,
}

impl IntcodeComputer {
    /// Gets the arguments for the current instruction and validates the pointers
    fn get_instruction(&self) -> (usize, usize, usize) {
        assert_in_bounds!(
            self.memory,
            self.pc + 3,
            "Reached end of program without halt: {}",
            self.pc + 3
        );

        let left_ptr = self.memory[self.pc + 1];
        let right_ptr = self.memory[self.pc + 2];
        let store_ptr = self.memory[self.pc + 3];

        assert_in_bounds!(
            self.memory,
            left_ptr,
            "Pointer out of bounds at {}: {}",
            self.pc,
            left_ptr
        );
        assert_in_bounds!(
            self.memory,
            right_ptr,
            "Pointer out of bounds at {}: {}",
            self.pc,
            right_ptr
        );
        assert_in_bounds!(
            self.memory,
            store_ptr,
            "Pointer out of bounds at {}: {}",
            self.pc,
            store_ptr
        );

        (left_ptr, right_ptr, store_ptr)
    }

    /// Executes the current instruction. If the program doesn't halt, the program counter is
    /// incremented.
    pub fn step(&mut self) {
        if self.halted {
            panic!("Tried to step through halted program");
        }

        assert_in_bounds!(self.memory, self.pc, "PC Overflow: {}", self.pc);

        let opcode = self.memory[self.pc];
        match opcode {
            1 => {
                let (left_ptr, right_ptr, store_ptr) = self.get_instruction();
                self.memory[store_ptr] = self.memory[left_ptr] + self.memory[right_ptr];
            }
            2 => {
                let (left_ptr, right_ptr, store_ptr) = self.get_instruction();
                self.memory[store_ptr] = self.memory[left_ptr] * self.memory[right_ptr];
            }
            99 => self.halted = true,
            _ => panic!(
                "Unexpected opcode at {}: {}\n{:?}",
                self.pc, opcode, self.memory
            ),
        };

        self.pc += 4;
    }

    /// Step through the program until a halt instruction is reached.
    pub fn execute(&mut self) {
        while !self.halted {
            self.step();
        }
    }
}
