use std::{
    collections::HashMap,
    fs::File,
    io::{prelude::*, BufReader},
};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

/// A note about this solution:
///
/// For my input, there aren't any vertical segments with the same x-value or horizontal segments
/// with the same y-value, HOWEVER, I didn't see anything in the prompt that indicated this is the
/// case for all possible inputs. Therefore, this solution accounts for the case where you have two
/// or more colinear segments.

fn main() -> Result<()> {
    // Get the list of segments for each line
    let file = File::open("input")?;
    let input = BufReader::new(file)
        .lines()
        .collect::<std::result::Result<Vec<_>, _>>()?
        .iter()
        .map(|line| {
            line.split(',')
                .map(Into::into)
                .collect::<Vec<DirectedSegment>>()
        })
        .collect::<Vec<_>>();
    let line1 = &input[0];
    let line2 = &input[1];

    // Sort into horizontal & vertical segments
    let (line1_vert, line1_horiz) = lines_from_segments(line1);
    let (line2_vert, line2_horiz) = lines_from_segments(line2);

    // Compare line 1's vertical segments with line 2's horizontal segments
    let mut intersections = find_intersections(line1_vert, line2_horiz);
    intersections.extend(find_intersections(line2_vert, line1_horiz));

    // Find the closest intersection
    let closest = intersections
        .iter()
        .min_by_key(|Point(x, y)| x.abs() + y.abs())
        .unwrap();

    println!("Part 1: {}", closest.0.abs() + closest.1.abs());

    // Traverse the first line and record the # of steps to each intersection
    let line1_distances = distances_to_intersections(line1, &intersections);
    let line2_distances = distances_to_intersections(line2, &intersections);

    // Put the distances for line 1 into a hash table, indexed by point
    let mut distances = HashMap::new();
    for (distance, point) in line1_distances {
        distances.insert(point, distance);
    }

    // Add the distances from line 2 to the distances from line 1
    for (distance, point) in line2_distances {
        let d = distances.get_mut(&point).unwrap();
        *d += distance;
    }

    // Find the shortest total distance
    let shortest = distances.iter().min_by_key(|(_, &d)| d).unwrap();

    println!("Part 2: {}", shortest.1);

    Ok(())
}

fn distances_to_intersections<'a>(
    line: &'a [DirectedSegment],
    intersections: &'a [Point],
) -> Vec<(i32, &'a Point)> {
    // Index the indersections by x and y coordinates
    let intersections_by_x = map_points_by_x(&intersections);
    let intersections_by_y = map_points_by_y(&intersections);

    let mut distances: Vec<(i32, &Point)> = Vec::new();
    let mut current_point = Point(0, 0); // Start at the origin
    let mut current_distance = 0;

    // Traverse the line
    for segment in line {
        use DirectedSegment::*;
        match segment {
            Up(d) => {
                // Find the point at the other end of the segment
                let next_point = Point(current_point.0, current_point.1 + *d);
                // Check if there could be some intersections along this segment
                if let Some(inter_candidates) = intersections_by_x.get(&current_point.0) {
                    inter_candidates
                        .iter()
                        // Find each segment that actually intersects
                        .filter(|Point(_, inter_y)| {
                            current_point.1 <= *inter_y && *inter_y <= next_point.1
                        })
                        .for_each(|intersection| {
                            // Find the gap from the current point to the intersection
                            let gap = intersection.1 - current_point.1;
                            // The total distance to the intersection is the gap plus the total
                            // distance we've travelled so far
                            distances.push((gap.abs() + current_distance, intersection));
                        })
                }
                // Increment the total distance, and the next point becomes our current point
                current_distance += d;
                current_point = next_point;
            }
            Down(d) => {
                let next_point = Point(current_point.0, current_point.1 - *d);
                if let Some(inter_candidates) = intersections_by_x.get(&current_point.0) {
                    inter_candidates
                        .iter()
                        .filter(|Point(_, inter_y)| {
                            next_point.1 <= *inter_y && *inter_y <= current_point.1
                        })
                        .for_each(|intersection| {
                            let gap = intersection.1 - current_point.1;
                            distances.push((gap.abs() + current_distance, intersection));
                        })
                }
                current_distance += d;
                current_point = next_point;
            }
            Left(d) => {
                let next_point = Point(current_point.0 - *d, current_point.1);
                if let Some(inter_candidates) = intersections_by_y.get(&current_point.1) {
                    inter_candidates
                        .iter()
                        .filter(|Point(inter_x, _)| {
                            next_point.0 <= *inter_x && *inter_x <= current_point.0
                        })
                        .for_each(|intersection| {
                            let gap = intersection.0 - current_point.0;
                            distances.push((gap.abs() + current_distance, intersection));
                        })
                }
                current_distance += d;
                current_point = next_point;
            }
            Right(d) => {
                let next_point = Point(current_point.0 + *d, current_point.1);
                if let Some(inter_candidates) = intersections_by_y.get(&current_point.1) {
                    inter_candidates
                        .iter()
                        .filter(|Point(inter_x, _)| {
                            current_point.0 <= *inter_x && *inter_x <= next_point.0
                        })
                        .for_each(|intersection| {
                            let gap = intersection.0 - current_point.0;
                            distances.push((gap.abs() + current_distance, intersection));
                        })
                }
                current_distance += d;
                current_point = next_point;
            }
        }
    }

    distances
}

/// Given sets of vertical and horizontal lines, finds all points where some lines intersect.
fn find_intersections(
    vertical: HashMap<i32, Vec<VerticalLine>>,
    horizontal: HashMap<i32, Vec<HorizontalLine>>,
) -> Vec<Point> {
    vertical // Look at each set of vertical lines with the same x-coordinate
        .iter()
        .flat_map(|(x_value, vert_lines)| {
            // Find horizontal lines that cross this line's x-coordinate
            // These lines may intersect, but they could be outside the vertical range
            let candidates = horizontal
                .iter()
                .flat_map(|(_, horiz_lines)| {
                    horiz_lines
                        .iter()
                        .filter(|HorizontalLine(_, x1, x2)| x1 <= x_value && x_value <= x2)
                })
                .collect::<Vec<_>>();

            // Look at each line
            // There's probably a way to get rid of all the `.collect()`s, but I dont' care enough
            vert_lines
                .iter()
                .flat_map(|vert_line| {
                    // For each horizontal line that may intersect
                    candidates
                        .iter()
                        // Check that it does
                        .filter(|HorizontalLine(y, _, _)| vert_line.1 <= *y && *y <= vert_line.2)
                        // Find the intersection point
                        .map(|horiz_line| Point(vert_line.0, horiz_line.0))
                        // Ignore the origin
                        .filter(|Point(x, y)| !(*x == 0 && *y == 0))
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
        })
        .collect()
}

/// Given a list of points, creates a lookup by their x-coordinates
fn map_points_by_x(points: &[Point]) -> HashMap<i32, Vec<&Point>> {
    let mut rtn = HashMap::new();

    for point in points {
        rtn.entry(point.0).or_insert_with(Vec::new).push(point);
    }

    rtn
}

/// Given a list of points, creates a lookup by their y-coordinates
fn map_points_by_y(points: &[Point]) -> HashMap<i32, Vec<&Point>> {
    let mut rtn = HashMap::new();

    for point in points {
        rtn.entry(point.1).or_insert_with(Vec::new).push(point);
    }

    rtn
}

#[derive(Debug)]
enum DirectedSegment {
    Up(i32),
    Down(i32),
    Left(i32),
    Right(i32),
}

impl From<&str> for DirectedSegment {
    /// Converts a string of the form 'R2' or 'L82' to a `DirectedSegment`
    fn from(value: &str) -> Self {
        use DirectedSegment::*;

        // Get the direction from the first char
        let dir = value.chars().nth(0).unwrap();
        // Get the distance from the remaining chars
        let distance = &value[1..].parse::<i32>().unwrap();

        match dir {
            'U' => Up(*distance),
            'D' => Down(*distance),
            'L' => Left(*distance),
            'R' => Right(*distance),
            _ => panic!("Bad value for direction: {}", dir),
        }
    }
}

/// (x, y)
#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
struct Point(i32, i32);

/// (x, y1, y2), y1 < y2
#[derive(Debug)]
struct VerticalLine(i32, i32, i32);

/// (y, x1, x2), x1 < x2
#[derive(Debug)]
struct HorizontalLine(i32, i32, i32);

/// Traverses a list of `DirectedSegment`s, grouping the lines in the list into horizontal and
/// vertical lines, hashed by their y and x coordinates, respectively.
fn lines_from_segments(
    segments: &[DirectedSegment],
) -> (
    HashMap<i32, Vec<VerticalLine>>,
    HashMap<i32, Vec<HorizontalLine>>,
) {
    let mut vert: HashMap<i32, Vec<VerticalLine>> = HashMap::new();
    let mut horiz: HashMap<i32, Vec<HorizontalLine>> = HashMap::new();

    // Start at the origin
    let mut current_point = Point(0, 0);

    for segment in segments {
        use DirectedSegment::*;
        match segment {
            Up(d) => {
                // Find the point at the end of this segment
                let next_point = Point(current_point.0, current_point.1 + *d);
                // Create the line, represented by its contant x-coordinate and its two
                // y-coordinates.
                let line = VerticalLine(current_point.0, current_point.1, next_point.1);
                // Insert it into the set of vertical lines with this x-coordinate
                vert.entry(current_point.0)
                    .or_insert_with(Vec::new)
                    .push(line);
                // The next point becomes the current point.
                current_point = next_point;
            }
            Down(d) => {
                let next_point = Point(current_point.0, current_point.1 - *d);
                let line = VerticalLine(current_point.0, next_point.1, current_point.1);
                vert.entry(current_point.0)
                    .or_insert_with(Vec::new)
                    .push(line);
                current_point = next_point;
            }
            Left(d) => {
                let next_point = Point(current_point.0 - *d, current_point.1);
                let line = HorizontalLine(current_point.1, next_point.0, current_point.0);
                horiz
                    .entry(current_point.0)
                    .or_insert_with(Vec::new)
                    .push(line);
                current_point = next_point;
            }
            Right(d) => {
                let next_point = Point(current_point.0 + *d, current_point.1);
                let line = HorizontalLine(current_point.1, current_point.0, next_point.0);
                horiz
                    .entry(current_point.0)
                    .or_insert_with(Vec::new)
                    .push(line);
                current_point = next_point;
            }
        }
    }

    (vert, horiz)
}
