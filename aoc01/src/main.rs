use std::{
    fs::File,
    io::{prelude::*, BufReader},
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let req_fuel = |mass: i32| mass / 3 - 2;

    let total_req_fuel = |mass: i32| {
        let mut total = req_fuel(mass);
        let mut next = total;
        loop {
            next = req_fuel(next);
            if next <= 0 {
                break;
            }
            total += next;
        }
        total
    };

    // Part 1
    let file = File::open("input")?;
    let buf_reader = BufReader::new(file);

    let part1 = buf_reader
        .lines()
        .map(|line| line.unwrap().parse::<i32>().unwrap())
        .map(req_fuel)
        .sum::<i32>();
    println!("Part 1: {}", part1);

    // Part 2
    let file = File::open("input")?;
    let buf_reader = BufReader::new(file);

    let part2 = buf_reader
        .lines()
        .map(|line| line.unwrap().parse::<i32>().unwrap())
        .map(total_req_fuel)
        .sum::<i32>();
    println!("Part 2: {}", part2);

    Ok(())
}
